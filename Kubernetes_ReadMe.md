# KuberNetes Basics:
- It is the virtualization based containerization tool or service which enables the fetuares like system adminstration and container orchastration.
- It is most used with docker and uses YAML file structure(like docker-compose.yaml if sounds familiar)

- the basics of kuberentes can be found here.
    [kubernetes basics from red hat and basic terminology](https://www.redhat.com/en/topics/containers/learning-kubernetes-tutorial)

- In linux, Kuberenetes is to be run with the help of minikube. Cluster creating to deployment refer to:
    [Kubernetes official website](https://www.redhat.com/en/topics/containers/learning-kubernetes-tutorial)

- Kubernetes yaml file basics:
    [what is yaml and kubernetes](https://www.mirantis.com/blog/introduction-to-yaml-creating-a-kubernetes-deployment/)

    [Digital Ocean Kubernetes guide on different topics(links from DO)](https://www.digitalocean.com/community/tutorial_series/from-containers-to-kubernetes-with-node-js)

- Ohter than basic topics, one will need to understand:
    [Persistent volume and volume types-official website](https://kubernetes.io/docs/concepts/storage/persistent-volumes/)


- For Kick starter of docker and kubernetes, refere to the <Microservice with nodejs Course> wherein there are ddedicated lacture towards both
    [Command cheat sheet](https://kubernetes.io/docs/reference/kubectl/docker-cli-to-kubectl/)
    - most useful commands are:
        - getting resources (deployment, services, pods)

        - getting resources from perticular namespace

        - applying configuration

        - rollout restart for Deployments
        
        - executing commands in pods with "exec"


* for sample yaml file for configuration of Deployments, Services, Persistent Volume(PV), PV Claim(PVC), ingress configuration SEE KUBECTL.zip (tested successfully on Digital Ocean managed Kubernetes cluster)
